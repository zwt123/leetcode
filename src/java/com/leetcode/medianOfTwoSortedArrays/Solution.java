package com.leetcode.medianOfTwoSortedArrays;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

You may assume nums1 and nums2 cannot be both empty.
 */
public class Solution {

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        double result = 0;

        //将nums1设置为长度最长的那个数组
        if(nums1.length < nums2.length){
            int [] tmp = nums1;
            nums1= nums2;
            nums2 = tmp;
        }

        if(nums2.length == 0){
            if(nums1.length % 2 == 0){
                result = ((double) nums1[nums1.length / 2] + (double) nums1[nums1.length / 2 - 1]) / 2;
            }else{
                result = nums1[nums1.length / 2];
            }
            return result;
        }

        int posL = (nums1.length+nums2.length) / 2 - 1;    //left指针
        int posR = 0;    //right指针

        while(nums1[posL] > nums2[posR]){
            posL--;
            posR++;
        }

        if((nums1.length+nums2.length) % 2 == 0){
            result = ((double) nums1[posL] + (double)nums2[posR]) / 2;
        }else {
            result = nums2[posR] > nums1[posL + 1] ? nums1[posL + 1] : nums2[posR];
        }
        return result;
    }
}
