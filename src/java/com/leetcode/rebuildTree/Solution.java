package com.leetcode.rebuildTree;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {

    public TreeNode buildTree(int[] preorder, int[] inorder) {

        if(preorder.length == 0)
            return null;

        Map<Integer, Integer> poses = new HashMap<Integer, Integer>();
        int length = inorder.length;
        for(int i = 0 ; i < length; i++){
            poses.put(inorder[i],i);
        }

        int root = preorder[0];
        int posOfRoot = poses.get(root);

        TreeNode now = new TreeNode(root);
        TreeNode left = null;
        TreeNode right = null;

        if (posOfRoot > 0)
            left = buildTree(preorder, inorder, poses,0,posOfRoot - 1, 1);
        if (posOfRoot < length - 1)
            right = buildTree(preorder, inorder, poses,posOfRoot+1,length - 1, posOfRoot+1);
        now.right = right;
        now.left = left;
        return now;
    }

    public TreeNode buildTree(int[] preorder, int[] inorder, Map<Integer, Integer> poses, int posL, int posR, int root){

        if(root >= preorder.length)
            return null;
        TreeNode now = new TreeNode(preorder[root]);
        TreeNode left = null;
        TreeNode right = null;

        if(posR - posL <= 2 && posR - posL > 0){
            if(posL < inorder.length && posL >= 0)
                left = new TreeNode(inorder[posL]);
            if(posR < inorder.length && posR >= 0)
                right = new TreeNode(inorder[posR]);
        }else if(posR - posL > 2){
            //左节点
            left = buildTree(preorder, inorder, poses, posL, poses.get(preorder[root])-1, root+1);

            //右节点
            right = buildTree(preorder, inorder, poses, poses.get(preorder[root])+1, posR, root+1);
        }

        now.right = right;
        now.left = left;

        CountDownLatch a = new CountDownLatch(12);
        a.await();
        a.countDown();

        return now;


    }
}
