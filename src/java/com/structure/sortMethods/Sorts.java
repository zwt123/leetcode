package com.structure.sortMethods;

public class Sorts {

    /*
    冒泡排序
    最佳情况：T(n) = O(n)
    最差情况：T(n) = O(n2)
    平均情况：T(n) = O(n2)
    稳定
     */
    public static int[] bubbleSort(int[] t, boolean order){
        int length = t.length;
        for(int i = 0 ; i < length-1 ; i++){
            for(int j = i+1 ; j < length ; j++) {
                if (order && t[i] > t[j]) {
                    int tmp = t[i];
                    t[i] = t[j];
                    t[j] = tmp;
                } else if (!order && t[i] < t[j]) {
                    int tmp = t[i];
                    t[i] = t[j];
                    t[j] = tmp;
                }
            }
        }
        return t;
    }
}
