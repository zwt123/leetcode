package com.structure.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BPlus {

    public static int MAX_SIZE = 3;

    public class Data implements Comparable<Data> {
        Integer value;
        String key;

        public Data(String key, Integer value) {
            this.value = value;
            this.key = key;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        @Override
        public int compareTo(Data data) {
            return this.value.compareTo(data.value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Data data = (Data) o;
            if(((Data) o).key != null){
                return Objects.equals(key, data.key);
            }
            else{
                return Objects.equals(value, data.value);
            }
        }
    }

    public class NodeOf23 {
        Data [] datas = new Data[MAX_SIZE];    //节点
        NodeOf23 []  childNodes = new NodeOf23[MAX_SIZE + 1];    //下层节点
        NodeOf23 pre;    //上层节点

        public NodeOf23(NodeOf23 pre) {
            this.pre = pre;
        }
        // 判断是否是叶子节点,因为左右等高
        public boolean isLeaf() {
            return childNodes[0] == null;
        }
        // 判断是否是顶点
        public boolean isTop() {
            return pre == null;
        }
        // 判断是否有位置
        public boolean isFull(){
            return childNodes.length == MAX_SIZE;
        }
        // 得到父节点
        public synchronized NodeOf23 getFather(){
            return pre;
        }
        // 查找某节点的位置
        public synchronized int indexOf(int value) throws Exception {
            Data inquiry = new Data(null, value);
            int indexOfMid = MAX_SIZE / 2;
            int start = 0;
            int end = MAX_SIZE;
            if(value == datas[indexOfMid].value){
                return indexOfMid;
            }else if(value > datas[indexOfMid].value){
                start = indexOfMid+1;
            }else {
                end = indexOfMid;
            }
            for(int i = start ; i < end ; i++){
                if(datas[i].value == value)
                    return i;
            }
            return -1;
        }
        // 得到data
        public synchronized Data find(int value) throws Exception {
            int index = indexOf(value);
            if(index == -1){
                return null;
            }
            else{
                return datas[index];
            }
        }
        // 插入data
        public synchronized void add(String key, int value) throws Exception{
            if(key == null){
                throw new Exception("无效值");
            }
            if()
            Data newData = new Data(key, value);

        }
        // 删除节点
        public synchronized void remove(Data data) throws Exception{
            if(data == null){
                throw new Exception("无效值");
            }
            if (this.datas == null) {
                throw new Exception("节点为空");
            }
            if(!this.datas.contains(data)){
                throw new Exception("无此节点");
            }
            this.datas.remove(data);
        }
        // 得到俩个key之间的下层节点
        public synchronized NodeOf23 getNextNodeBetweenDatas(Data left, Data right){
            if(left == null && right == null) return null;
            NodeOf23 result = null;

            if(left == null)  result = childNodes[0];
            if(right == null) result = childNodes[MAX_SIZE];
            for(int i = 0 ; i < MAX_SIZE; i++ ){
                if(childNodes[i].equals(left))
                    result = childNodes[i+1];
            }
            return result;
        }
        // 删除俩个key之间的下层节点
        public synchronized void removeNextNodeBetweenDatas(Data left, Data right){
            if(left == null) childNodes[0] = null;
            if(right == null) childNodes[MAX_SIZE] = null;
            else childNodes.remove(this.datas.indexOf(left) + 1);
        }
    }
}
